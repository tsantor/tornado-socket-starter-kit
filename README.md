# Tornado Socket Starter Kit
Tim Santor <tsantor@xstudios.com>

# Overview
A quick head start on creating a socker server and/or socker client.

## Setup development environment
As a developer I assume you have `pyenv` and `make` installed.

```bash
make env
make reqs
```

## Run examples
### Server
```bash
make serve
```

## View the Dashboard
The dashboard will show you a simple web page with the details of each connected client. Open a browser window and visit: [http://0.0.0.0:8080](http://0.0.0.0:8080)

### Client
```bash
make listen
```

> **NOTE:** You can open terminal windows and run as many clients as you want. Ensure you create a config file for each client and give them a unique `id`.
