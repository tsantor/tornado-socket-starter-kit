# -*- coding: utf-8 -*-

import json
import logging
import uuid
from datetime import datetime

logger = logging.getLogger(__name__)

# -----------------------------------------------------------------------------


class MessageFormatError(Exception):
    """Raised when a message is not the proper JSON format of:
    {"type":"foo", "data":"bar"}."""

    pass


class Message(object):
    """Constructs a JSON message"""

    def __init__(self, data_dict):
        """Inject common data into all message."""
        self.msg_dict = {}
        self.msg_dict["id"] = uuid.uuid4().hex
        self.msg_dict["date"] = datetime.now().strftime("%a, %b %d, %Y")
        self.msg_dict["time"] = datetime.now().strftime("%I:%M:%S %p")
        self.update(data_dict)

    def update(self, msg_dict):
        """Update the message with new information."""
        # Merge the dictionaries
        updated_dict = self.msg_dict.copy()
        updated_dict.update(msg_dict)

        self.msg_dict = updated_dict
        return self

    def encode(self):
        """Return encoded JSON message"""
        return json.dumps(self.msg_dict) + "\n"


def message_handler(client, msg):
    """Determines how messages are handled."""
    try:
        obj = json.loads(msg)
        if "type" not in obj:
            raise MessageFormatError()
    except (ValueError, MessageFormatError):
        logger.error("Invalid message received from %s: %s", client.identifier, msg)
        return

    if obj["type"] == "Ping":
        client.on_ping()

    elif obj["type"] == "Pong":
        client.on_pong()

    elif obj["type"] == "KeepAlive":
        logger.debug("Keep Alive message received from %s", client.identifier)

    else:
        logger.debug("Unhandled message received from %s: %s", client.identifier, msg)
