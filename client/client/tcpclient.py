# -*- coding: utf-8 -*-

import logging
from datetime import datetime

from tornado import gen
from tornado.ioloop import IOLoop, PeriodicCallback
from tornado.iostream import StreamClosedError
from tornado.tcpclient import TCPClient

from .message import Message

logger = logging.getLogger(__name__)

# -----------------------------------------------------------------------------


class SimpleTcpClient:
    """
    Client socket

    The PeriodicCallback keep_alive will periodically send a message to the
    server every X seconds. If the connection is closed, it will attempt to reconnect.

    ping_interval and ping_timeout work together to constantly ping the server.
    If the ping times out, it will close the connection.
    """

    def __init__(
        self,
        ip,
        port,
        message_handler,
        ping_interval,
        ping_timeout,
        keepalive_interval,
    ):
        self.ip = ip
        self.port = port
        self.message_handler = message_handler
        self.ping_interval = ping_interval
        self.ping_timeout = ping_timeout

        # ID should be overwritten with a more human friendly name
        self.id = None

        self.ping_time = 0
        self.stream = None

        self.connect()
        if keepalive_interval:
            if keepalive_interval < 30:
                raise ValueError("keepalive_interval must be >= 30")
            PeriodicCallback(self.keep_alive, keepalive_interval * 1000).start()

    @property
    def identifier(self):
        """Return string identifier for logging."""
        return f"{self.id} ({self.ip}:{self.port})"

    @property
    def time_online(self):
        """Calculate the time this client has been online."""
        delta = datetime.now() - self.ini_time
        s = delta.total_seconds()
        hours, remainder = divmod(s, 3600)
        minutes, seconds = divmod(remainder, 60)
        return "%02d:%02d:%02d" % (hours, minutes, seconds)

    @gen.coroutine
    def connect(self):
        """Connect."""
        logger.debug("Trying to connect to %s:%s", self.ip, self.port)
        try:
            self.stream = yield TCPClient().connect(self.ip, self.port)
            self.ini_time = datetime.now()
        except Exception as err:
            logger.warning("Connection error: %s", err)
        else:
            logger.debug('Connected as "%s"', self.id)
            self.handshake()
            self.start_pinging()
            self.run()

    def disconnect(self):
        """Disconnect."""
        self.stop_pinging()
        if self.stream:
            self.stream.close()
            self.stream = None

    @gen.coroutine
    def run(self):
        """Run the client."""
        while True:
            try:
                data = yield self.stream.read_until(b"\n")
                self.message_handler(self, data.decode("utf-8").strip())
                # yield self.stream.write(data)
            except StreamClosedError:
                logger.info(
                    "Connection closed. Total time online: %s", self.time_online
                )
                self.stream = None
                break

    def keep_alive(self):
        """Keep connection alive."""
        if self.stream is None:
            self.connect()

    def send_message(self, message):
        """Send a message to the server."""
        try:
            self.stream.write(message.encode("utf-8"))
        except StreamClosedError as err:
            logger.warning(err)

    def handshake(self):
        """Introduce ourself to the server."""
        msg = Message({"type": "Handshake", "data": self.id}).encode()
        self.send_message(msg)

    # ----------

    def write_ping(self):
        """Send ping."""
        msg = Message({"type": "Ping"}).encode()
        self.send_message(msg)

    def on_pong(self):
        """Invoked when a pong message is received from the server."""
        now = IOLoop.current().time()
        self.last_pong = now

        # Calculate ping time
        self.ping_time = round((now - self.last_ping) * 1000, 3)
        logger.debug("Received pong from server. Ping time: %s ms", self.ping_time)

    def on_ping(self):
        """Invoked when a ping message is received from the server."""
        # logger.debug("Received ping from server")
        msg = Message({"type": "Pong"}).encode()
        self.send_message(msg)

    def start_pinging(self):
        """Start sending periodic pings to keep the connection alive."""
        if self.ping_interval > 0:
            self.last_ping = self.last_pong = IOLoop.current().time()
            self.ping_callback = PeriodicCallback(
                self.periodic_ping, self.ping_interval * 1000
            )
            self.ping_callback.start()

    def periodic_ping(self):
        """Send a ping to keep the websocket alive.
        Called periodically if the ping_interval is set and non-zero.
        """
        # We may not have gotten a "on_disconnect" event so let's check if
        # we should stop pinging before attempting to ping
        if self.stop_pinging():
            return

        # Check for timeout on pong
        now = IOLoop.current().time()
        since_last_pong = now - self.last_pong
        if since_last_pong > self.ping_timeout:
            logger.warning("Ping timeout from server")
            self.stream.close()
            return

        self.write_ping()
        self.last_ping = now

    def stop_pinging(self):
        """Stop pinging and return True if stopped."""
        if not self.stream and self.ping_callback is not None:
            logger.debug("Stop periodic ping for server")
            self.ping_callback.stop()
            return True
