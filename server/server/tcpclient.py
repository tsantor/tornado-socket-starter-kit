# -*- coding: utf-8 -*-

import logging
import socket
import uuid
from datetime import datetime

from tornado import gen
from tornado.ioloop import IOLoop, PeriodicCallback
from tornado.iostream import StreamClosedError

from .message import Message

logger = logging.getLogger(__name__)

# -----------------------------------------------------------------------------


class SimpleTcpClient:
    """
    Server SimpleTcpClient

    When a client connects an instance of this handler is spawned.
    """

    def __init__(
        self,
        stream,
        message_handler,
        client_manager,
        ping_interval,
        ping_timeout,
        drop_dupes=False,
    ):

        # Initial id for the client, id is set during the "Handshake" message
        self.id = uuid.uuid4().hex
        self.uid = self.id

        self.stream = stream
        self.message_handler = message_handler
        self.client_manager = client_manager
        self.ping_interval = ping_interval
        self.ping_timeout = ping_timeout
        self.drop_dupes = drop_dupes

        self.ping_time = 0

        self.stream.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        self.stream.socket.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)

        self.stream.set_close_callback(self.on_disconnect)

    def to_obj(self):
        """Used to get the object properties in the dashboard template."""
        return {
            "name": self.id,
            "ip": self.ip,
            "port": self.port,
            "ping": self.ping_time,
            "time_online": self.time_online,
            "uid": self.uid,
        }

    @property
    def identifier(self):
        """Return string identifier for logging."""
        return f"{self.id} ({self.ip}:{self.port})"

    @property
    def time_online(self):
        """Calculate the time this client has been online."""
        delta = datetime.now() - self.ini_time
        s = delta.total_seconds()
        hours, remainder = divmod(s, 3600)
        minutes, seconds = divmod(remainder, 60)
        return "%02d:%02d:%02d" % (hours, minutes, seconds)

    def handle_message(self, message):
        """When a message is received"""
        self.message_handler(self, message)

    @gen.coroutine
    def on_connect(self):
        self.ip, self.port = self.stream.socket.getpeername()
        self.ini_time = datetime.now()

        self.client_manager.add(self)

        # We know a client has connected, but they have not introduced
        # themselves just yet so "id" is not known
        logger.info("Client connected: %s", self.identifier)
        logger.info("Clients connected: %s", len(self.client_manager.clients))

        self.start_pinging()

        yield self.dispatch_client()

    @gen.coroutine
    def on_disconnect(self):
        self.client_manager.remove(self)

        self.stop_pinging()

        logger.info(
            "Client disconnected: %s. Total time online: %s",
            self.identifier,
            self.time_online,
        )
        logger.info("Clients connected: %s", len(self.client_manager.clients))
        yield []

    @gen.coroutine
    def dispatch_client(self):
        "https://blog.stephencleary.com/2009/04/message-framing.html"

        while True:
            try:
                data = yield self.stream.read_until(b"\n")
                self.handle_message(data.decode("utf-8").strip())
            except StreamClosedError:
                self.stream = None
                break

    def send_message(self, message):
        """Send a message to the server."""
        try:
            self.stream.write(message.encode("utf-8"))
        except StreamClosedError as err:
            logger.warning(err)

    def handshake(self, client_id):
        """The client has fully introduced themselves."""
        old_id = self.id
        self.id = client_id
        logger.debug(
            "Handshake message received from %s (%s => %s)",
            self.identifier,
            old_id,
            self.id,
        )

        # Do not allow a client id to connect more than once
        if self.drop_dupes and self.client_manager.count_by_id(self.id) > 1:
            existing_client = self.client_manager.get_by_id(self.id)
            logger.warning(
                "There is already a client %s. Rejecting connection from %s.",
                existing_client.identifier,
                self.identifier,
            )
            self.stream.close()
            return

    # ----------

    def write_ping(self):
        """Send ping."""
        msg = Message({"type": "Ping"}).encode()
        self.send_message(msg)

    def on_pong(self):
        """Invoked when a pong message is received from the client."""
        now = IOLoop.current().time()
        self.last_pong = now

        # Calculate ping time
        self.ping_time = round((now - self.last_ping) * 1000, 3)
        # logger.debug(
        #     "Received pong from %s. Ping time: %s ms", self.identifier, self.ping_time
        # )

    def on_ping(self):
        """Invoked when a ping message is received from the a client."""
        # logger.debug("Received ping from %s", self.identifier)
        msg = Message({"type": "Pong"}).encode()
        self.send_message(msg)

    def start_pinging(self):
        """Start sending periodic pings to keep the connection alive."""
        if self.ping_interval > 0:
            self.last_ping = self.last_pong = IOLoop.current().time()
            self.ping_callback = PeriodicCallback(
                self.periodic_ping, self.ping_interval * 1000
            )
            self.ping_callback.start()

    def periodic_ping(self):
        """Send a ping to keep the websocket alive.
        Called periodically if the ping_interval is set and non-zero.
        """
        # We may not have gotten a "on_disconnect" event so let's check if
        # we should stop pinging before attempting to ping
        if self.stop_pinging():
            return

        # Check for timeout on pong
        now = IOLoop.current().time()
        since_last_pong = now - self.last_pong
        if since_last_pong > self.ping_timeout:
            logger.warning("Ping timeout from %s", self.identifier)
            self.stream.close()
            return

        self.write_ping()
        self.last_ping = now

    def stop_pinging(self):
        """Stop pinging and return True if stopped."""
        if not self.stream and self.ping_callback is not None:
            # logger.debug("Stop periodic ping for %s", self.identifier)
            self.ping_callback.stop()
            return True
