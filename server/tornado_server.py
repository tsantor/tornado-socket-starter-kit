# -*- coding: utf-8 -*-

import logging
import os
import signal

from tornado import gen
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.tcpserver import TCPServer
from tornado.web import Application

from server import config
from server.clientmanager import ClientManager
from server.message import message_handler
from server.tcpclient import SimpleTcpClient
from server.web import DashboardHandler, DashboardUpdateHandler

logger = logging.getLogger(__name__)

# -----------------------------------------------------------------------------

client_manager = ClientManager()


class SimpleTcpServer(TCPServer):
    """Simple TCP Server."""

    @gen.coroutine
    def handle_stream(self, stream, address):
        """Called for each new connection, stream.socket is
        a reference to socket object"""
        ping_interval = config.getint("default", "ping_interval")
        ping_timeout = None
        if config.has_option("default", "ping_timeout"):
            ping_timeout = config.getint("default", "ping_timeout")
            if ping_timeout < ping_interval:
                raise Exception("ping_timeout must be greater than ping_interval")

        connection = SimpleTcpClient(
            stream,
            message_handler,
            client_manager,
            ping_interval,
            ping_timeout,
            drop_dupes=config.getboolean("default", "drop_dupes"),
        )
        yield connection.on_connect()


# -----------------------------------------------------------------------------


def _handle_shutdown_signals(signum, frame):
    assert signum in (signal.SIGINT, signal.SIGTERM)
    logger.info("signal -%d received", signum)
    logger.info("Received graceful shutdown request")
    IOLoop.current().stop()


def get_tornado_app():
    """Return a tornado web application instance which includes url config"""
    settings = dict(
        template_path=os.path.join(os.path.dirname(__file__), "server/templates"),
        static_path=os.path.join(os.path.dirname(__file__), "server/static"),
        debug=True,
    )

    urls = [
        # (r'/', DefaultHandler),
        (
            r"/",
            DashboardHandler,
            dict(
                readpoints=[],  # config.items('read_points'),
                clients=client_manager.clients,
            ),
        ),
        (r"/update", DashboardUpdateHandler, dict(clients=client_manager.clients)),
    ]

    return Application(urls, **settings)


def main():
    # Things seem to fail more gracefully if we trigger the stop
    # out of band (with a signal handler) instead of catching the
    # KeyboardInterrupt...
    signal.signal(signal.SIGINT, _handle_shutdown_signals)
    signal.signal(signal.SIGTERM, _handle_shutdown_signals)

    # Get config items
    host = config.get("default", "ip")
    port = config.getint("default", "port")

    # TCP server
    server = SimpleTcpServer()
    server.listen(port, host)
    logger.debug("Listening on %s:%d..." % (host, port))

    # Get config items
    http_host = config.get("http", "ip")
    http_port = config.getint("http", "port")

    # HTTP server
    app = get_tornado_app()
    httpserver = HTTPServer(app)
    httpserver.listen(http_port, http_host)

    IOLoop.current().start()

    server.stop()
    httpserver.stop()
    logger.info("Server shut down gracefully")


if __name__ == "__main__":
    main()
